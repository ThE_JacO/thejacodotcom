#include "getDeepNeighbors.h"
#include <maya/MFnPlugin.h>

//__declspec(dllexport)
MStatus initializePlugin(MObject pluginObj){
    MStatus status = MS::kSuccess;
    MFnPlugin plugin(pluginObj, "Raff", "1.0", "any");

    status = plugin.registerCommand("getDeepNeighbors",
                                    GetDeepNeighbors::creator,
                                    GetDeepNeighbors::cmdSyntax);

    return status;
}

//__declspec(dllexport)
MStatus uninitializePlugin(MObject pluginObj){
    MStatus status = MS::kSuccess;
    MFnPlugin plugin(pluginObj);

    status = plugin.deregisterCommand("getDeepNeighbors");

    return status;
}