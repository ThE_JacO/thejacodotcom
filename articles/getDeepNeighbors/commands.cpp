#include "getDeepNeighbors.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>

#include <maya/MItMeshVertex.h>

const char *longFlag_depth = "-depth", *shortFlag_depth = "-dp",
           *longFlag_distance = "-distance", *shortFlag_distance = "-dst";

MSyntax GetDeepNeighbors::cmdSyntax(){
    MSyntax syntax;
    return syntax;
}



void GetDeepNeighbors::initFromArgs(const MArgDatabase& argData){
    nbrdDepth = 1;
    distanceLimit = 0.0;

    double tmpFlag = 0.0;
    if(argData.isFlagSet(longFlag_depth)){
        argData.getFlagArgument(longFlag_depth, 0, tmpFlag);
        if(tmpFlag < 1.0){
            nbrdDepth = 1;
            MGlobal::displayWarning("neighborhood depth flag (-dp) was set to less than 1. Forcing to 1");
        }
        else{
            nbrdDepth = (unsigned int)tmpFlag;
        }
    }

    if(argData.isFlagSet(longFlag_distance)){
        argData.getFlagArgument(longFlag_distance, 0, distanceLimit);
    }
}


MStatus GetDeepNeighbors::validateSelection(){
    MGlobal::getActiveSelectionList(userSelection);
    userSelection.getDagPath(0, dagToSelection, selectedComponents);

    if(dagToSelection.apiType() != MFn::kMesh){
        unsigned int shapeCount = 0;
        dagToSelection.numberOfShapesDirectlyBelow(shapeCount);
        if(shapeCount != 1){
            MGlobal::displayWarning("invalid number of shape nodes under the selected item");
            return MS::kFailure;
        }

        dagToSelection.extendToShape();
        if(!dagToSelection.hasFn(MFn::kMesh)){
            MGlobal::displayWarning("shape found but not of type Mesh");
            return MS::kFailure;
        }
    }

    return MS::kSuccess;
}


MStatus GetDeepNeighbors::doIt(const MArgList& args){
    MStatus cmdStatus = MS::kSuccess;
    MArgDatabase argData(cmdSyntax(), args);

    initFromArgs(argData);

    cmdStatus = validateSelection();
    if(cmdStatus != MS::kSuccess){return cmdStatus;}

    MItMeshVertex itrMesh(dagToSelection, selectedComponents);


    
    return cmdStatus;
}