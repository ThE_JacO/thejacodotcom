#pragma once

#include <maya/MPxCommand.h>
#include <maya/MArgDatabase.h>

#include <maya/MObject.h>
#include <maya/MDagPathArray.h>
#include <maya/MDagPath.h>
#include <maya/MSelectionList.h>

class GetDeepNeighbors: public MPxCommand{
public:
    GetDeepNeighbors::GetDeepNeighbors(){};
    virtual GetDeepNeighbors::~GetDeepNeighbors(){};

    bool isUndoable() const{
        return false;
    }

    static void* creator(){
        return new GetDeepNeighbors();
    }

    static MSyntax cmdSyntax();

    virtual MStatus doIt(const MArgList&);

private:
    MSelectionList userSelection;
    MDagPathArray filteredSelection;
    MDagPath dagToSelection;
    MObject selectedComponents;
    //MObject shapeNode;

    unsigned int nbrdDepth;
    double distanceLimit;

    void initFromArgs(const MArgDatabase& argData);
    MStatus validateSelection();
};